//
//  TravelListViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import FirebaseDatabase
import Firebase
class TravelListViewController: UIViewController, UITableViewDelegate , UITableViewDataSource, StopListViewControllerDelegate  {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var travels : [Travel] = []
    
    let user = Auth.auth().currentUser
    var travel: Travel?
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIBarButtonItem(image: UIImage (named: "Shape"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(customClickedAction(_:)))
        
        self.navigationItem.leftBarButtonItem = logo
        tableView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        tableView.delegate = self
        tableView.dataSource = self
        let xib = UINib(nibName: "TravelViewCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "TravelViewCell")
        tableView.tableFooterView = UIView() // способ который убират полоски в таблице
        tableView.separatorStyle = .none
        
        
        navigationItem.title = "Путешествия"
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(customClicked(_:)))
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        
        
        downloadAndParseTravels { (travels) in
            
            for travel in travels {
                if travel.userId == self.user?.uid {
                    self.travels = travels
                }
              
                var travel = travel
                self.downloadStops(for: travel) { (stops) in
                    travel.stops = stops
                }
                
            }
            
            
             self.tableView.reloadData()
        }
        
        
        
    }
    func downloadStops( for travel: Travel, onComplete: (([Stop]) -> ())? ) {
        let database = Database.database().reference()
        database.child("stops").queryOrdered(byChild: "travelId").queryEqual(toValue: travel.id).observeSingleEvent(of: .value) { (response) in
            if let value = response.value as? [String: Any] {
                var result: [Stop] = []
                for item in value.values {
                    if let stopJson = item as? [String:Any],
                       var  id =  stopJson["id"] as? String,
                       var travelId = stopJson["travelId"] as? String,
                       var name = stopJson["name"] as? String,
                       var rating = stopJson["rating"] as? Int,
                       var spentmoney = stopJson["spentmoney"] as? Int,
                       var latitude = stopJson["latitude"] as? Double,
                       var longitude = stopJson["longitude"] as? Double,
                       var transport = stopJson["transport"] as? Int,
                       var currency = stopJson["currency"] as? String,
                       var description = stopJson["description"] as? String {
                        var stop = Stop(id: id,
                                        travelId: travelId,
                                        name: name,
                                        raiting: rating,
                                        spentMoney: Double(spentmoney),
                                        description: description,
                                        transport: Transport(rawValue: transport)! ,
                                        latitude: latitude,
                                        longitude: longitude,
                                        spentMoneyCourency: Currency(rawValue: currency) ?? .dollar)
                        result.append(stop)
                    }
                    
                    
                }
                onComplete?(result)
            }
            
        }
        
    }
    func downloadAndParseTravels(onComplete:(([Travel]) -> ())? ) {
        let database = Database.database().reference()
        database.child("travels").observeSingleEvent(of: .value) { response in
            if let value = response.value as? [String:Any] {
                var result: [Travel] = []
                for item in value.values {
                    if let travelJson = item as? [String:Any] {
                        if let id = travelJson["id"] as? String,
                           let name = travelJson["name"] as? String,
                           let description = travelJson["description"] as? String,
                           let userID = self.user?.uid {
                            let travel = Travel(id: id,
                                                userId: userID ,
                                                name: name,
                                                description: description,
                                                stops: [],
                                                raiting: [])
                            result.append(travel)
                            
                            
                        }
                    }
                }
                onComplete?(result)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TravelViewCell") as! TravelViewCell
        print(cell.travel?.raiting)
        
        
        cell.travelNameLAbel.text = "\(travels[indexPath.row].name)"
        cell.travelDescriptionLabel.text = "\(travels[indexPath.row].description)"
        
        
        
        //let rating = travels[indexPath.row].avaradgeRaiting
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopVC = StopListViewController()
        stopVC.delegate = self
        stopVC.travel = travels[indexPath.row]
        
        
        
        navigationController?.pushViewController(stopVC, animated: true)
    }
    
    @objc func customClickedAction(_ sender: Any) {
        let profileVC = ProfileViewController()
        navigationController?.pushViewController(profileVC, animated: true)
    }
    @objc func customClicked(_ sender: Any) {
        let alertVC = UIAlertController(title: "Добавить новое путешествие", message: "", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (action) in
            print("OK clicked")
            if  let userId = Auth.auth().currentUser?.uid {
                let travel = Travel(id: UUID().uuidString,
                                    userId: userId ,
                                    name: alertVC.textFields![0].text ?? "",
                                    description: alertVC.textFields![1].text ?? "",
                                    stops: [],
                                    raiting: [])
                
                self.travels.append(travel)
                self.tableView.reloadData()
                
                let json = ["id": travel.id,
                            "userId": userId,
                            "name": travel.name,
                            "description": travel.description
                ]
                let database = Database.database().reference()
                let child = database.child("travels").child("\(travel.userId)")
                child.setValue(json) {(error , ref) in
                }
            }
        }
        alertVC.addAction(okAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            print("cancel clicked")
        }
        alertVC.addAction(cancelAction)
        
        alertVC.addTextField { (textField) in
            textField.placeholder = "Введите название путешествия..."
            textField.textColor = .blue
            
        }
        alertVC.addTextField { (textField) in
            textField.placeholder = "Введите описание путешествия"
            textField.textColor = .red
            
        }
        present(alertVC, animated: true, completion: nil)
    }
    func stopListViewControllerDelegate(travelStops: Travel) {
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            travels[selectedIndexPath.row] = travelStops
            
            tableView.reloadRows(at: [selectedIndexPath], with: .none)
        } else {
            
            tableView.reloadData()
        }
        
    }
    
    
    
}
