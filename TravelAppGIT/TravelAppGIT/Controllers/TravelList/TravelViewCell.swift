//
//  TravelViewCell.swift
//  TravelAppGIT
//
//  Created by mac on 2/23/21.
//

import UIKit

class TravelViewCell: UITableViewCell {
    @IBOutlet weak var imageStar5: UIImageView!
    @IBOutlet weak var imageStar4: UIImageView!
    @IBOutlet weak var imageStar3: UIImageView!
    @IBOutlet weak var imageStar2: UIImageView!
    @IBOutlet weak var imageStar1: UIImageView!
    @IBOutlet weak var travelDescriptionLabel: UILabel!
    @IBOutlet weak var travelNameLAbel: UILabel!
    @IBOutlet weak var viewCell: UIView!
    
    var images : [UIImageView] = []
    var travel: Travel? {
        didSet{
            travelNameLAbel.text = travel?.name
            travelDescriptionLabel.text = travel?.description
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        viewCell.layer.cornerRadius = 10
        images.append(imageStar1)
        images.append(imageStar2)
        images.append(imageStar3)
        images.append(imageStar4)
        images.append(imageStar5)
        
    }

    
    
}
