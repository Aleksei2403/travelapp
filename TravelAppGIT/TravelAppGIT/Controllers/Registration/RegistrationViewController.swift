//
//  RegistrationViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import Firebase
class RegistrationViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    
    @objc func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageBack = UIImage(named: "back")
        registrationButton.layer.cornerRadius = 9
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageBack, style: .done, target: self, action: #selector(customBackClicked(_:)))
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    
    @IBAction func registrationClicked(_ sender: UIButton) {
        if let email = emailTextField.text, let password = passwordTextfield.text {
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let travelVC = TravelListViewController()
                    self.navigationController?.pushViewController(travelVC, animated: true)
                } else {
                    let errorMassege = error?.localizedDescription ?? "Error"
                    let alertVc = UIAlertController(title: nil, message: errorMassege , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertVc.addAction(action)
                    self.present(alertVc, animated: true, completion: nil)
                }
            }
        }
    }
}
