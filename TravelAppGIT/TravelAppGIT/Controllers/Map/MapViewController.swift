//
//  MapViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func mapControllerSelectLocation(latitude: Double, longitude: Double)
}

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    
    var stop : Stop?
    var lat : Double?
    var lon : Double?
    var delegate: MapViewControllerDelegate?
    @objc func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let latitude = lat, let longitude = lon {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            mapView.addAnnotation(annotation)
        }
        let longTapRecongnizer = UILongPressGestureRecognizer(target: self, action: #selector(longTap(sender:)))
        mapView.addGestureRecognizer(longTapRecongnizer)
        
        let imageBack = UIImage(named: "back")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageBack, style: .done, target: self, action: #selector(customBackClicked(_:)))
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    @objc func longTap(sender: UIGestureRecognizer) {
        switch sender.state {
        
        case .possible:
            print("possible")
        case .began:
            print("began")
            mapView.removeAnnotations(mapView.annotations)
            let locationInView = sender.location(in: mapView)
            let locationOnMap = mapView.convert(locationInView, toCoordinateFrom: mapView)
            let annotation = MKPointAnnotation()
            annotation.coordinate = locationOnMap
            mapView.addAnnotation(annotation)
            delegate?.mapControllerSelectLocation(latitude: locationOnMap.latitude, longitude: locationOnMap.longitude)
        case .changed:
            print("changed")
        case .ended:
            print("ended")
        case .cancelled:
            print("cancelled")
        case .failed:
            print("failed")
        @unknown default:
            print("default")
        }
    }
}
