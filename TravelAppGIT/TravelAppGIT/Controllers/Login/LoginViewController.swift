//
//  LoginViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import Firebase
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var textEmail: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var eysButton: UIButton!
    
    @IBAction func loginClicked(_ sender: Any) {
//        var email = "alex"
//        var password = "1234"
//        if password  == textPassword.text && email == textEmail.text {
//            navigationController?.pushViewController(TravelListViewController(), animated: true)
//        } else {
//            viewEmail.backgroundColor = .red
//            labelEmail.textColor = .red
//            viewPassword.backgroundColor = .red
//            labelPassword.tintColor = .red
//        }
        if let email = textEmail.text , let password = textPassword.text {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let travelVC = TravelListViewController()
                    self.navigationController?.pushViewController(travelVC, animated: true)
                } else {
                    let errorMassege = error?.localizedDescription ?? "Error"
                    let alertVc = UIAlertController(title: nil, message: errorMassege , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertVc.addAction(action)
                    self.present(alertVc, animated: true, completion: nil)
                }
            }
        }
    }
    @IBAction func forgotButtonClicked(_ sender: UIButton) {
        let forgotVC = ForgotPasswordViewController()
        navigationController?.pushViewController(forgotVC, animated: true)
        
    }
    
    @IBAction func eysChange(_ sender: UIButton) {
        sender.isSelected.toggle()
        textPassword.isSecureTextEntry.toggle()
        print("+++")
    }
    
    @objc func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = 9
        textPassword.isSecureTextEntry = true
        eysButton.backgroundColor = .white
        let imageNormal = UIImage.init(named: "hide")
        let imageSelected = UIImage.init(named: "View")
        let imageBack = UIImage(named: "back")
        eysButton.setImage(imageNormal, for: .normal)
        eysButton.setImage(imageSelected, for: .selected)
        eysButton.addTarget(self, action: #selector(eysChange(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageBack, style: .done, target: self, action: #selector(customBackClicked(_:)))
        navigationItem.leftBarButtonItem?.tintColor = .black
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.layoutIfNeeded()
        
    }
}
