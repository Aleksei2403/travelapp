//
//  SpentMoneyViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
protocol  SpentMoneyDelegete {
    func data(moneyCurrency : Currency, money : String)
}
class SpentMoneyViewController: UIViewController {
    
    
    @IBOutlet weak var gotovoButton: UIButton!
    @IBOutlet weak var segmentedMoney: UISegmentedControl!
    @IBOutlet weak var textSumm: UITextField!
    
    var stop : Stop?
    var segmentedIndex: String?
    var currency : Currency = .dollar
    var delegate : SpentMoneyDelegete?
   
    @IBAction func buttonClicked(_ sender: UIButton) {
        delegate?.data( moneyCurrency: currency , money: textSumm.text ?? "")
        dismiss(animated: true, completion: nil)
        print("+++")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let index = segmentedMoney.selectedSegmentIndex
 
        if let stop = stop {
            textSumm.text = "\(stop.spentMoney)"
        }
        
            switch  stop?.spentMoneyCourency {
            case .dollar:
                segmentedMoney.selectedSegmentIndex = 0
            case .euro:
                segmentedMoney.selectedSegmentIndex = 1
            case .ruble:
                segmentedMoney.selectedSegmentIndex = 2
            case .none:
                break
            }
          
        gotovoButton.layer.cornerRadius = 4
        textSumm.layer.cornerRadius = 4
        
        segmentedMoney.setTitleTextAttributes([.foregroundColor: UIColor.init(named: "Color")], for: .normal)
        segmentedMoney.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        segmentedMoney.layer.cornerRadius = 4
        segmentedMoney.backgroundColor = .white
        segmentedMoney.selectedSegmentTintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        
    }
    @IBAction func segmentedController(_ sender: UISegmentedControl) {
        switch(sender.selectedSegmentIndex){
            case 0:
                currency = .dollar
                
            case 1:
                currency = .euro
                
            case 2:
                currency = .ruble
                
            default:
                break
            }
        
        
    }
}




