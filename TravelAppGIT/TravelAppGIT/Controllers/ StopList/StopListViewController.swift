//
//  StopListViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/10/21.
//

import UIKit
protocol StopListViewControllerDelegate {
    func stopListViewControllerDelegate(travelStops: Travel)
}

class StopListViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, CreateStopViewControllerDelegete{
    
    @IBOutlet weak var tableView: UITableView!
    
    var travel : Travel!
    
    var delegate: StopListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(customBackClicked(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(customAddClicked(_:)))
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        view.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        title = travel.name
        tableView.delegate = self
        tableView.dataSource = self
        let xib = UINib(nibName: "StopListCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "StopListCell")
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return travel.stops.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StopListCell") as! StopListCell
        travel.raiting.append(travel.stops[indexPath.row].raiting)
        
        cell.travelStop = travel.stops[indexPath.row]
        switch travel.stops[indexPath.row].spentMoneyCourency {
        case .dollar:
            cell.spentSummLabel.text = "$\(travel.stops[indexPath.row].spentMoney)"
        case .euro :
            cell.spentSummLabel.text = "€\(travel.stops[indexPath.row].spentMoney)"
        case .ruble :
            cell.spentSummLabel.text = "₽\(travel.stops[indexPath.row].spentMoney)"
        }
        switch travel.stops[indexPath.row].transport {
        case .car:
            cell.imageTransport?.image = UIImage(named: "car")
        case .airplane :
            cell.imageTransport?.image = UIImage(named: "somalet")
        case .train :
            cell.imageTransport?.image = UIImage(named: "train")
            
        }
        
        switch travel.stops[indexPath.row].raiting {
        case  0 :
            cell.imageStar1.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar2.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar3.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar4.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar5.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        case 1:
            cell.imageStar1.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar2.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar3.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar4.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar5.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        case 2:
            cell.imageStar1.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar2.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar3.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar4.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar5.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        case 3:
            cell.imageStar1.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar2.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar3.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar4.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
            cell.imageStar5.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        case 4:
            cell.imageStar1.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar2.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar3.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar4.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar5.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        case 5:
            cell.imageStar1.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar2.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar3.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar4.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
            cell.imageStar5.tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let createVC = CreateStopViewController()
        createVC.stop = travel.stops[indexPath.row]
        createVC.travel = travel
        createVC.delegate = self
        navigationController?.pushViewController(createVC, animated: true)
        
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            tableView.beginUpdates()
            travel.stops.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    func saveDate(stop1: Stop) {
        travel.stops.append(stop1)
        tableView.reloadData()
        
    }
    func changeDate(stop1: Stop) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            travel.stops[selectedIndexPath.row] = stop1
            tableView.reloadRows(at: [selectedIndexPath], with: .none)
        } else {
            travel.stops.append(stop1)
            tableView.reloadData()
        }
    }
    @objc  func customBackClicked(_ sender: Any) {
        let travelVC = TravelListViewController()
       
            delegate?.stopListViewControllerDelegate(travelStops: travel )
           
        navigationController?.popViewController(animated: true)
    }
    @objc func customAddClicked(_ sender: Any) {
        let createVC = CreateStopViewController()
        createVC.travel = travel
        createVC.delegate = self
        navigationController?.pushViewController(createVC, animated: true)
    }
    
    
    
}

