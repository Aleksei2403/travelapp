//
//  StopListCell.swift
//  TravelAppGIT
//
//  Created by mac on 3/1/21.
//

import UIKit

class StopListCell: UITableViewCell {
    @IBOutlet weak var imageStar5: UIImageView!
    @IBOutlet weak var imageStar4: UIImageView!
    @IBOutlet weak var imageStar3: UIImageView!
    @IBOutlet weak var imageStar2: UIImageView!
    @IBOutlet weak var imageStar1: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var viewCell: UIView!
    
    @IBOutlet weak var imageTransport: UIImageView!
    @IBOutlet weak var raitingLabel: UIImageView!
    @IBOutlet weak var spentSummLabel: UILabel!
    @IBOutlet weak var deskLabel: UILabel!
    var images:[UIImageView] = []
    var travelStop: Stop? {
        didSet {
            nameLabel.text = travelStop?.name
            deskLabel.text = travelStop?.description
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        images.append(imageStar1)
        images.append(imageStar1)
        images.append(imageStar1)
        images.append(imageStar1)
        images.append(imageStar1)
        viewCell.layer.cornerRadius = 10
    }

    
}
