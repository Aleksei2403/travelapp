//
//  SplashViewController.swift
//  TravelAppGIT
//
//  Created by mac on 3/30/21.
//

import UIKit
import Firebase
class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.fetchAndActivate { (status, error) in
            DispatchQueue.main.async {
                let welcomeVc = WelcomeViewController()
                self.navigationController?.pushViewController(welcomeVc, animated: true)
            }
            
        }
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
}
