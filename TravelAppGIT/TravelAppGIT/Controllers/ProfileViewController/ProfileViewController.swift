//
//  ProfileViewController.swift
//  TravelAppGIT
//
//  Created by mac on 3/18/21.
//

import UIKit
import MessageUI
import Firebase

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate,MFMailComposeViewControllerDelegate{
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Profile"
        let back = UIBarButtonItem(image: UIImage (named: "back"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(customClickedAction(_:)))
        
        self.navigationItem.leftBarButtonItem = back
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let blur = UIBlurEffect(style: .dark)
        let blurView = UIVisualEffectView(effect: blur)
        blurView.frame = view.bounds
        view.addSubview(blurView)
        view.sendSubviewToBack(blurView)
        view.sendSubviewToBack(backgroundImage)
    
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
        //self.navigationController?.navigationBar.isTranslucent = true
        
        self.navigationController?.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    @objc func customClickedAction(_ sender: Any) {
        let travelVC = TravelListViewController()
        navigationController?.pushViewController(travelVC, animated: true)
    }
    @IBAction func buttonExitAction(_ sender: UIButton) {
        logoutUser()
    }
    
    @IBAction func addPhotoButtonClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func writeLetterButtonClicked(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                // Configure the fields of the interface.
                composeVC.setToRecipients(["address@example.com"])
                composeVC.setSubject("Hello!")
                composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
                // Present the view controller modally.
                self.present(composeVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func appInfoButtonClicked(_ sender: Any) {
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            imageView.contentMode = .scaleToFill
            imageView.image = pickedImage
        
            backgroundImage.image = imageView.image
            imageView.layer.cornerRadius = imageView.frame.size.width / 2
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func logoutUser() {
        // call from any screen
        
        do { try Auth.auth().signOut() }
        catch { print("already logged out") }
        
        for vc in self.navigationController!.viewControllers {
            if let myViewCont = vc as? WelcomeViewController
            {
                self.navigationController?.popToViewController(myViewCont, animated: true)
            }
        }
    }
}

