//
//  CollectionViewCell.swift
//  TravelAppGIT
//
//  Created by mac on 3/22/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var viewWithImage: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewWithImage.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
    }

    @IBAction func deleteImage(_ sender: Any) {
    }
}
