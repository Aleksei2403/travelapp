//
//  CreateStopViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import FirebaseDatabase
protocol  CreateStopViewControllerDelegete {
    func saveDate( stop1 : Stop)
    func changeDate( stop1 : Stop)
}


class CreateStopViewController: UIViewController, UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MapViewControllerDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
   
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var viewMinus: UIView!
    @IBOutlet weak var viewPlus: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var coordinateLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var labelSumm: UILabel!
    @IBOutlet weak var raitingLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var travel : Travel!
    var stop : Stop?
    var latitude: Double?
    var longitude: Double?
    var transport : Transport?
    var delegate : CreateStopViewControllerDelegete?
    var courency : Currency?
    var arrayImage:[UIImage] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: Bundle(for: CollectionViewCell.self)), forCellWithReuseIdentifier: "CollectionViewCell")
        if let stop = stop {
            nameTextField.text = stop.name
            raitingLabel.text = "\(stop.raiting)"
            coordinateLabel.text = "\(stop.latitude), \(stop.longitude)"
            labelSumm.text = "\(stop.spentMoney)"
            descriptionTextView.text = "\(stop.description)"

            switch  stop.transport {
            case .airplane:
                segmentedControl.selectedSegmentIndex = 0
            case .train:
                segmentedControl.selectedSegmentIndex = 1
            case .car:
                segmentedControl.selectedSegmentIndex = 2

            }
        }
        viewMinus.layer.cornerRadius =  7
        viewPlus.layer.cornerRadius = 7
        viewMinus.layer.maskedCorners = [.layerMinXMinYCorner , .layerMinXMaxYCorner]
        viewPlus.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        viewPlus.layer.borderWidth = 1
        viewPlus.layer.borderColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        viewMinus.layer.borderWidth = 1
        viewMinus.layer.borderColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        
        
        navigationItem.title = "Остановка"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Oтмена", style: .done, target: self, action: #selector(customBackClicked(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .done, target: self, action: #selector(passDataToStopVC(_:)))
        navigationItem.leftBarButtonItem?.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        segmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)
        segmentedControl.setTitleTextAttributes([.foregroundColor: UIColor.white ], for: .selected)
        segmentedControl.layer.borderWidth = 1.0
        segmentedControl.layer.borderColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        segmentedControl.selectedSegmentTintColor = #colorLiteral(red: 0.5137254902, green: 0.537254902, blue: 0.9098039216, alpha: 1)
        segmentedControl.imageForSegment(at: 0)
        self.segmentedControl.backgroundColor = .white
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayImage.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 60, height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        cell.imageView.image = arrayImage[indexPath.row]
        
        return cell
    }
    @IBAction func buttonMinStepper(_ sender: UIButton) {
        if raitingLabel.text == "0" {
            raitingLabel.text = "0"
        } else if raitingLabel.text == "1" {
            raitingLabel.text = "\(1 - 1)"
        }else if raitingLabel.text == "2" {
            raitingLabel.text = "\(2 - 1 )"
        }else if raitingLabel.text == "3" {
            raitingLabel.text = "\(3 - 1)"
        }else if raitingLabel.text == "4" {
            raitingLabel.text = "\(4 - 1 )"
        }else if raitingLabel.text == "5" {
            raitingLabel.text = "\(5 - 1)"
        }
        
    }
    
    @IBAction func ButtonPlusStepper(_ sender: UIButton) {
        if raitingLabel.text == "0" {
            raitingLabel.text = "\(sender.tag + 1)"
        } else if raitingLabel.text == "1" {
            raitingLabel.text = "\(sender.tag + 2)"
        }else if raitingLabel.text == "2" {
            raitingLabel.text = "\(sender.tag + 3)"
        }else if raitingLabel.text == "3" {
            raitingLabel.text = "\(sender.tag + 4)"
        }else if raitingLabel.text == "4" {
            raitingLabel.text = "\(sender.tag + 5)"
        }
       
    }
    
    @IBAction func segmentedControlTransport(_ sender: UISegmentedControl) {
        switch(sender.selectedSegmentIndex){
            case 0:
                transport = .airplane
               
            case 1:
                transport = .train
                
            case 2:
                transport = .car
               
            default:
                break
            }
    }
    func mapControllerSelectLocation(latitude: Double, longitude: Double) {
        coordinateLabel.text = "(\(latitude), \(longitude))"
        self.latitude = latitude
        self.longitude = longitude
    }
    @IBAction func buttonMapClicked(_ sender: UIButton) {
        let mapVC = MapViewController()
        mapVC.lat = latitude
        mapVC.lon = longitude
        mapVC.delegate = self
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    @IBAction func buttonClickedSummView(_ sender: UIButton) {
        let summVC = SpentMoneyViewController()
        summVC.delegate = self
        summVC.stop = stop
        present(summVC, animated: true, completion: nil)
    }
    
    
    @objc func passDataToStopVC(_ sender: Any) {
        
        let stopAlreadyExist = stop == nil
        stop = Stop(id: UUID().uuidString,
                    travelId : travel.id,
                    name: nameTextField.text ?? "",
                     raiting: Int(raitingLabel.text ?? "") ?? 0,
                     spentMoney: Double(labelSumm.text ?? "") ?? 0,
                     description: descriptionTextView.text ?? "",
                     transport: transport ?? .airplane,
                     latitude: latitude ?? 0,
                     longitude: longitude ?? 0,
                     spentMoneyCourency: courency ?? .dollar)
        let json: [String : Any] = ["travelId":stop?.travelId,
                    "id":stop?.id,
                    "name": stop?.name,
                    "rating": stop?.raiting,
                    "spentmoney": stop?.spentMoney,
                    "latitude": stop?.latitude,
                    "longitude": stop?.longitude,
                    "transport": stop?.transport.rawValue,
                    "currency": stop?.spentMoneyCourency.rawValue,
                    "description": stop?.description,
        ]
        let database = Database.database().reference()
        let child = database.child("stops").child("\(stop?.id)")
        child.setValue(json) {(error , ref) in
            
        }
        
        if stopAlreadyExist == (stop != nil) {
            delegate?.saveDate(stop1: stop!)
            
        } else {
            delegate?.changeDate(stop1: stop!)
        }
        navigationController?.popViewController(animated: true)
    }
    
    @objc func customBackClicked(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addImageButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        print("photo add")
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            arrayImage.append(pickedImage)
            collectionView.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
   
}



extension CreateStopViewController : SpentMoneyDelegete {
    func data(moneyCurrency: Currency, money: String) {
        labelSumm.text = money
        courency = moneyCurrency
        
    }
}

