//
//  ForgotPasswordViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var forgotButton: UIButton!
    
    @objc func customBackClicked(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imageBack = UIImage(named: "back")
        forgotButton.layer.cornerRadius = 9
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: imageBack, style: .done, target: self, action: #selector(customBackClicked(_:)))
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
    }
}
