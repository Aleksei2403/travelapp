//
//  WelcomeViewController.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import Firebase
class WelcomeViewController: UIViewController {
    
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let remoteConfig = RemoteConfig.remoteConfig()
        let loginText = remoteConfig["login_button"].stringValue
        loginButton.setTitle(loginText, for: .normal)
        let registerText = remoteConfig["register_button"].stringValue
        createButton.setTitle(registerText, for: .normal)
        
        createButton.layer.cornerRadius = 9
        loginButton.layer.cornerRadius = 9
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
    }
    
    @IBAction func createButtonClicked(_ sender: Any) {
        let registrationVC = RegistrationViewController()
        navigationController?.pushViewController(registrationVC, animated: true)
        print("++++")
    }
    
    @IBAction func plusClicked(_ sender: Any) {
        let registrationVC = RegistrationViewController()
        navigationController?.pushViewController(registrationVC, animated: true)
        print("++++")
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        let loginVC = LoginViewController()
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
}
