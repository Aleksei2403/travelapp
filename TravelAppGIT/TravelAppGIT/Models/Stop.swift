//
//  Stop.swift
//  TravelAppGIT
//
//  Created by mac on 2/16/21.
//

import UIKit

enum Transport: Int {
    case car, train, airplane
}
enum Currency: String {
    case ruble = "₽"
    case euro =  "€"
    case dollar = "$"
}
struct  Stop {
    var id: String
    var travelId: String
    var name : String
    var raiting : Int
    var spentMoney : Double
    var description : String
    var transport : Transport
    var latitude : Double
    var longitude: Double
    var spentMoneyCourency : Currency
}
