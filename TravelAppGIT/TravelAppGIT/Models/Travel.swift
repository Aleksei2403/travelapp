//
//  Travel.swift
//  TravelAppGIT
//
//  Created by mac on 2/16/21.
//

import Foundation


struct Travel {
    var id : String
    var userId: String
    var name : String
    var description : String
    var stops : [Stop]
    var raiting: [Int] = [1]
    var avaradgeRaiting: Int {
        get {
            var sum = 1
            for value in raiting {
                sum += value
            }
            return Int( sum/raiting.count)
        }
    }
}


