//
//  AppDelegate.swift
//  TravelAppGIT
//
//  Created by mac on 2/9/21.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window : UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let welcomeVC = TravelListViewController()
                let navigationController = UINavigationController(rootViewController: welcomeVC)
                self.window?.rootViewController = navigationController
                self.window?.makeKeyAndVisible()
            } else {
                self.window = UIWindow(frame: UIScreen.main.bounds)
                let welcomeVC = WelcomeViewController()
                let navigationController = UINavigationController(rootViewController: welcomeVC)
                self.window?.rootViewController = navigationController
                self.window?.makeKeyAndVisible()
            }
        }
                
                
        
        // Override point for customization after application launch.
        
        return true
    }

}

